# 🔥Live & Tribe 🔥

Lien Heroku : https://concert-production.herokuapp.com

Lien de présentation vidéo : https://youtu.be/h1XqJ0j_oTg

## L'application 🎤


### Pourquoi donc ? 

Si vous êtes passionné(e)s de musique et que vous voyez votre artiste préféré en concert aux alentours de chez vous, combien de fois vous êtes vous retrouvé(e)s dans la situation où parce que vos goûts vous sont singuliers : 
* vous y allez en mode solo ;
* vous trainez plus ou moins de force des proches qui connaissent peu ou pas cet artiste (avec une possibilité de détruire une amitié)
Et si vous cherchez des gens pour y aller avec vous sur des réseaux sociaux, vous tomberez davantage sur des personnes qui vendent leurs places. 

Bref, la dimension communautaire offerte par les concerts n’est pas du tout exploitée. C’est regrettable pour l’expérience des fans ainsi que pour celle des artistes eux-mêmes, dont le modèle économique dépend de plus en plus des tournées et concerts.

Live & Tribe est une application qui veut permettre aux passionnés de musique de profiter des performances de leurs artistes préférés entre eux, du before au debrief.  

### Ça marche comment ? 

D’un point de vue technique, nous sommes particulièrement fiers de permettre deux types d’expériences utilisateurs pour les fans :

 #### L’expérience de recherche rapides et ciblées des concerts
 
  Pour se faire, on a réussi à prendre grâce à l'API de l’appli d'alerte de concerts Songkicks d’avoir accès aux milliers de concerts des plus grandes villes de France. Puis pour optimiser l’expérience de recherche, nous avons installé les fonctionnalités d’Algolia. 

Ainsi le concert désiré par le fan sera trouvé par notre application en quelques millisecondes. 

#### L’interaction communautaire et les discussions entre les fans de l’artiste

  Grâce à Action Cable, nous avons pu implémenter différents channels de discussions. 

Ainsi chaque fan inscrit peut échanger avec d’autres passionnés de conversation : proposition d’un before commun à côté de la salle de concert, débats sur le meilleur album de l’artiste, partage de contenus etc. 

### Et dans le futur ? 

Notre vision, c’est non seulement de solidifier les liens entre les fans d’artistes les plus passionnés dès qu’une date de concert est fixée, mais également d’aider les artistes à diversifier leurs sources de revenus grâce aux temps forts que constituent leurs concerts. 

Plus concrètement, les fans pourront ainsi acheter les produits dérivés de leurs artistes préférés sur les pages Artistes de l’appli “Live & Tribe” et venir retirer leur produit directement aux stands de merchandising. Ainsi, plus de queue de fans indécis devant les stands de merchandising et une meilleure gestion des stock de merchandising pour les équipes des artistes. 

A moyen long-terme, le modèle de revenus de “Live & Tribe” sera principalement de vendre les services et l’affluence de notre plateforme aux labels ou manager des artistes, et de multiplier les partenariats avec les acteurs numériques des industries musicales. 


## Notre tribe 👯

Cinq moussaillons de la session 5 de The Hacking Project :

* Soraya Fructuoso 
* Nicolas Zhao 
* David Belaga
* Pierre Tâm-Anh Le Khac 
* Amélie Foulquier

Nous remercions notre mentor Willy Duville pour toute son aide, sa patience et sa disponiblité durant les deux semaines de développement 💛


Nous espérons que vous apprécierez cette application, et n'oubliez pas de vous éclater en concert et de soutenir les artistes ❤️🔥🎸









