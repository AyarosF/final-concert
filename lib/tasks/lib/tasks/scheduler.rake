desc "This task is called by the Heroku scheduler add-on"
task :update_algolia => :environment do
  puts "Updating algolia_db..."
  AlgoliaScrapJob.perform_now
  puts "done."
end