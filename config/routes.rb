Rails.application.routes.draw do
  root 'static_pages#home'
  devise_for :users, controllers: { registrations: 'users/registrations' , omniauth_callbacks: 'users/omniauth_callbacks'}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :concerts
  post 'concerts/:id', to: 'concerts#reload'
  mount ActionCable.server, at: '/cable'
  resources :chatrooms, param: :slug # slug only change the default :id to :slug, value doesn't change
  resources :messages
  get "users/:id", to: "users#show", as: "users"
end
