# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
City.create(name: "Paris", metro_area_songkick_id: 28909)
City.create(name: "Lyon", metro_area_songkick_id: 28889)
City.create(name: "Marseille", metro_area_songkick_id: 156979)
City.create(name: "Toulouse", metro_area_songkick_id: 28930)
City.create(name: "Bordeaux", metro_area_songkick_id: 28851)
City.create(name: "Lille", metro_area_songkick_id: 28886)
City.create(name: "Nice", metro_area_songkick_id: 28903)
City.create(name: "Nantes", metro_area_songkick_id: 28901)
City.create(name: "Strasbourg", metro_area_songkick_id: 28928)
City.create(name: "Rennes", metro_area_songkick_id: 28916)
