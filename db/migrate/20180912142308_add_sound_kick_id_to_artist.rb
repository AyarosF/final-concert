class AddSoundKickIdToArtist < ActiveRecord::Migration[5.2]
  def change
    add_column :artists, :soundkick_id, :integer
  end
end
