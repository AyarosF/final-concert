class CreateJoinTableCityConcert < ActiveRecord::Migration[5.2]
  def change
    create_join_table :cities, :concerts do |t|
      # t.index [:city_id, :concert_id]
      # t.index [:concert_id, :city_id]
    end
  end
end
