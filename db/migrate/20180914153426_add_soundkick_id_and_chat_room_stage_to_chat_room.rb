class AddSoundkickIdAndChatRoomStageToChatRoom < ActiveRecord::Migration[5.2]
  def change
    add_reference :chatrooms, :concert, foreign_key: true
    add_column :chatrooms, :soundkick_id, :integer
    add_column :chatrooms, :stage, :integer
  end
end
