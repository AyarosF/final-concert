class CreateConcerts < ActiveRecord::Migration[5.2]
  def change
    create_table :concerts do |t|
      t.string :title
      t.datetime :date
      t.belongs_to :city, index: true 
      t.timestamps
    end
  end
end
