class AddSoundKickIdToConcerts < ActiveRecord::Migration[5.2]
  def change
    add_column :concerts, :soundkick_id, :integer
  end
end
