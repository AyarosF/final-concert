# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_14_153426) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "artists", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "name", default: "", null: false
    t.string "city", default: ""
    t.text "bio", default: ""
    t.string "avatar", default: ""
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "soundkick_id"
    t.index ["email"], name: "index_artists_on_email", unique: true
    t.index ["reset_password_token"], name: "index_artists_on_reset_password_token", unique: true
  end

  create_table "artists_concerts", id: false, force: :cascade do |t|
    t.bigint "artist_id", null: false
    t.bigint "concert_id", null: false
    t.index ["artist_id", "concert_id"], name: "index_artists_concerts_on_artist_id_and_concert_id"
    t.index ["concert_id", "artist_id"], name: "index_artists_concerts_on_concert_id_and_artist_id"
  end

  create_table "chatrooms", force: :cascade do |t|
    t.string "topic"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "concert_id"
    t.integer "soundkick_id"
    t.integer "stage"
    t.index ["concert_id"], name: "index_chatrooms_on_concert_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.integer "metro_area_songkick_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "algolia_index"
  end

  create_table "cities_concerts", id: false, force: :cascade do |t|
    t.bigint "city_id", null: false
    t.bigint "concert_id", null: false
  end

  create_table "concerts", force: :cascade do |t|
    t.datetime "date"
    t.bigint "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.integer "soundkick_id"
    t.index ["city_id"], name: "index_concerts_on_city_id"
  end

  create_table "concerts_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "concert_id", null: false
    t.index ["concert_id", "user_id"], name: "index_concerts_users_on_concert_id_and_user_id"
    t.index ["user_id", "concert_id"], name: "index_concerts_users_on_user_id_and_concert_id"
  end

  create_table "messages", id: :serial, force: :cascade do |t|
    t.string "content"
    t.integer "user_id"
    t.integer "chatroom_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chatroom_id"], name: "index_messages_on_chatroom_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "searches", force: :cascade do |t|
    t.string "title"
    t.string "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.string "city", default: ""
    t.text "bio", default: ""
    t.string "avatar", default: ""
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider"
    t.string "uid"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "chatrooms", "concerts"
  add_foreign_key "messages", "chatrooms"
  add_foreign_key "messages", "users"
end
