class AlgoliaScrapJob < ApplicationJob
  queue_as :default

  def perform
    require 'net/http'
    require 'json'
    require 'rubygems'
    require 'algoliasearch'
    require 'objspace'

    Algolia.init(application_id: 'DA0L98N7JW',
                 api_key:        Rails.application.credentials.dig(:algolia_api))

    index = Algolia::Index.new('ParisConcerts')
    index.clear_index
    index.set_settings(
      searchableAttributes: [
        'displayName',
        'location',
      ],
      customRanking: [
        'desc(popularity)'
      ]
    )

    @sndk = Rails.application.credentials.dig(:songkick_api)

    City.all.each do |city|
      @location = city.metro_area_songkick_id.to_s
      p city.name
      50.times do |i|
        result = JSON.parse(Net::HTTP.get(URI.parse("https://api.songkick.com/api/3.0/metro_areas/" + @location + "/calendar.json?page=#{i + 1}&apikey=" + @sndk)))["resultsPage"]["results"]["event"]
        clean_result = []
        if result.nil?
          break
        else
          result.each do |elem|
            if elem.to_s.size > 10_000
              puts "Skipped element of size", elem.to_s.size
            else
              clean_result << elem
            end
          end
          puts i
          index.add_objects(clean_result)
        end
      end
    end
  end
end
