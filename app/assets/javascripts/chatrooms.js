$(document).on('turbolinks:load', function() {
  return $("#message_content").keyup(function() {
    if ($("#message_content").val() !== "") {
      return $("#submit_button").show();
    } else {
      return $("#submit_button").hide();
    }
  });
});
