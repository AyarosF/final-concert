class ContactMailer < ApplicationMailer

    include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
    default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views

    default from: 'concertstation.thp@gmail.com'

  def welcome_email(user)
    @user = user
    mail to: user.email, subject: 'Welcome to our fabulous site'
  end

end
