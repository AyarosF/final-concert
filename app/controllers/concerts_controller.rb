require 'net/http'
require 'json'

class ConcertsController < ApplicationController
  def index
  end

  def create # s'inscrire depuis une page concert
    concert = Concert.find_or_initialize_by(soundkick_id: params[:soundkick_id])
    concert.date = Date.parse(params[:date]).iso8601
    concert.city = City.find_by(metro_area_songkick_id: params[:city])
    concert.soundkick_id = params[:soundkick_id]
    concert.title = params[:concert_title]
    concert.save
    if !concert.users.exists?(current_user.id)
      concert.users << User.find(current_user.id)
    end
    if !concert.chatrooms.exists?
      list_topic = ["Before","Trajet, covoiturage...","Discussion : les vrais savent","Debrief"]
      list_topic.each do |topic|
        chat = Chatroom.new
        chat.topic = topic
        chat.soundkick_id = params[:soundkick_id]
        chat.concert_id = concert.id
        chat.stage = 1
        chat.save
      end
    end
    redirect_to concert_path(concert.soundkick_id)
  end

  def reload
    current_user.concerts.delete(Concert.find_by(soundkick_id: params[:id]))
    redirect_to concert_path(params[:id])
  end

  def show
    @event_id = params[:id]
    if Concert.exists?(soundkick_id: params[:id])
      @concert = Concert.find_by(soundkick_id: params[:id])
      @interested_number = @concert.users.all.length
      @concert_exist = true
      if user_signed_in?
        @inscrit = @concert.users.exists?(current_user.id)
      end
    end
    resulte = JSON.parse(Net::HTTP.get(URI.parse("https://api.songkick.com/api/3.0/events/#{@event_id}.json?apikey="+ Rails.application.credentials.dig(:songkick_api))))
    result = resulte["resultsPage"]["results"]["event"]
    @artist_name = result["performance"][0]["displayName"]
    @venue_place = result["venue"]["displayName"]
    @venue_adress = result["venue"]["street"]
    @venue_postal_code = result["venue"]["zip"]
    @venue_city = result ["venue"]["city"]["displayName"]
    @venue_city_id = result["venue"]["city"]["id"]
    @venue_date = result["start"]["date"]
    @venue_hour = result["start"]["time"]
    @concert_url = result["uri"]
    @concert_name = result["displayName"]
    @sentences = ["Organisez vos before pour faire connaissance", "Trouvez d'autres personnes pour effectuer votre trajet", "Les top album, les dernières infos, les clash... Les vrais savent.", "Partagez vos souvenirs et impressions post-concert"]
  end
end
