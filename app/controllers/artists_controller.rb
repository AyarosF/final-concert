class ArtistsController < ApplicationController

  def show
    @artists = Artist.find(params[:id])
  end

end
