class ChatroomsController < ApplicationController
  # def index
  #   @chatrooms = Chatroom.paginate(page: params[:page], per_page: 5)
  # end
  #
  # def new
  #   @chatroom = Chatroom.new
  # end
  #
  # def create
  #   @chatroom = Chatroom.new(chatroom_params)
  #   if @chatroom.save
  #     respond_to do |format|
  #       format.html { redirect_to @chatroom }
  #       format.js
  #     end
  #   else
  #     respond_to do |format|
  #       flash[:notice] = {error: ["a chatroom with this topic already exists"]}
  #       format.html { redirect_to new_chatroom_path }
  #       format.js { render template: 'chatrooms/chatroom_error.js.erb'}
  #     end
  #   end
  # end
  #
  # def show
  #   @chatroom = Chatroom.find_by(id: params[:slug])
  #   @message = Message.new
  # end
  #
  # private
  #
  # def chatroom_params
  #   params.require(:chatroom).permit(:topic)
  # end

    def index
      @chatrooms = Chatroom.paginate(page: params[:page], per_page: 5)
    end

    def show
      @chatroom = Chatroom.find_by(id: params[:slug])
      @message = Message.new
    end
end
