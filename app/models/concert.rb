class Concert < ApplicationRecord
  has_and_belongs_to_many :artists
  has_and_belongs_to_many :users
  belongs_to :city
  has_many :chatrooms
end
