class Chatroom < ApplicationRecord
  has_many :messages, class_name: "Message", dependent: :destroy
  has_many :user, through: :messages
  belongs_to :concert
end
