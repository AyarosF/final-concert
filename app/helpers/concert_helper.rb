module ConcertHelper
  def artiste_show(search_artist)
    require 'net/http'
    require 'json'
    result = JSON.parse(Net::HTTP.get(URI.parse("https://api.songkick.com/api/3.0/events.json?&artiste_name=" + search_artist + "&location=sk:28909&apikey=" + Rails.application.credentials.dig(:songkick_api) + "&per_page=100&page=" + (i + 1).to_s)))["resultsPage"]["results"]["event"]
    result.each { |key|
      hash = {}
      hash["titre"] = key["displayName"].downcase
      hash["date"] = key["start"]["date"].downcase
      hash["ville"] = key["location"]["city"].downcase
      hash["lieu"] = key["venue"]["displayName"].downcase
      hash["url"] = key["uri"]
      array_artiste = []
      key["performance"].each { |performance|
        array_artiste << performance["displayName"].downcase
      }

      hash["artistes"] = array_artiste

      test.push(hash)
    }
    return test
  end
end
